# Minimap in Unity #
Project files for our tutorial on creating a Minimap in Unity.

Check out our [YouTube Channel](https://www.youtube.com/channel/UC4F_FNgEM8g6Hxl7PYlSDzA) for more tutorials.

### Copyright ##
This project is released into the public domain. For more information see [LICENSE](./LICENSE.md).